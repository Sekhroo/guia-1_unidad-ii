#!/usr/bin/env python3
#-*- coding:utf-8 -*-
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class Dlg_Msg_Info():

    def __init__(self,a,b,registros):
        builder = Gtk.Builder()
        builder.add_from_file("./UI/guia-1_inter.ui")

        self.msg_info = builder.get_object("msg_info_acep")
        self.msg_info.set_title("Recapitulación")

        self.label_one_entry = builder.get_object("regis_entry_1")

        registros.append(a)
        registros.append(b)

        regis_result = ",".join(registros)

        if a == "" and b == "":
            regis_result =". . ."

        if a == ". . ." and b == ". . .":
            for i in reversed(registros):
                del registros[i]           

        self.label_one_entry.set_text(regis_result)

        self.msg_info.show_all()
