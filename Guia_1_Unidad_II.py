#!/usr/bin/env python3
#-*- coding:utf-8 -*-
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from dlg_warning import Dlg_Warning
from dlg_msg_info import Dlg_Msg_Info

class MainWindow():
    def __init__(self,registros):
        builder = Gtk.Builder()
        builder.add_from_file("./UI/guia-1_inter.ui")
        
        interfaz = builder.get_object("inter_guia-1.ui")
        interfaz.connect("destroy", Gtk.main_quit)
        interfaz.set_title("Guía 1 - Unidad II")
        interfaz.resize(800, 600)

        btn_aceptar = builder.get_object("btn_func_aceptar")
        btn_aceptar.connect("clicked", self.on_btn_aceptar)

        btn_reset = builder.get_object("btn_func_reset")
        btn_reset.connect("clicked", self.on_btn_reset)

        self.label_one = builder.get_object("label_text_one")
        self.label_two = builder.get_object("label_text_two")
        self.label_result = builder.get_object("label_text_result")

        self.entry_one = builder.get_object("texto_entry_one")
        self.entry_two = builder.get_object("texto_entry_two")

        interfaz.show_all()
    
    def on_btn_aceptar(self, btn=None):
        comentario1 = self.entry_one.get_text()
        comentario2 = self.entry_two.get_text()

        try:
            for i  in range(1000):
                if i == comentario1:
                    a = int(i)
            for i  in range(1000):
                if i == comentario2:
                    b = int(i)
            result = int(a) + int(b)
        except:
            result = comentario1 + comentario2
        if self.label_one == ". . .":
            comentario1 = ". . ."
            comentario2 = ". . ."

        dlg_info = Dlg_Msg_Info(comentario1, comentario2, registros)
        resp = dlg_info.msg_info.run()

        if resp == Gtk.ResponseType.ACCEPT:
            dlg_info.msg_info.destroy()

        self.label_one.set_text(comentario1)
        self.label_two.set_text(comentario2)
        self.label_result.set_text(result)
                
        self.entry_one.set_text("")
        self.entry_two.set_text("")

    def on_btn_reset(self, btn=None):
        dlg = Dlg_Warning()
        resp = dlg.warning.run()
        
        if resp == Gtk.ResponseType.CANCEL:
            dlg.warning.destroy()
        else:
            self.label_one.set_text(". . .")
            self.label_two.set_text(". . .")
            self.label_result.set_text(". . .")
            
            a = ""
            b = ""
            registros = []

            dlg_info = Dlg_Msg_Info(a, b, registros)
            resp = dlg_info.msg_info.run()

            if resp == Gtk.ResponseType.ACCEPT:
                dlg_info.msg_info.destroy()

            dlg.warning.destroy()  


if __name__ == "__main__":
    registros = []
    MainWindow(registros)
    Gtk.main()
    