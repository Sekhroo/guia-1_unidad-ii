#!/usr/bin/env python3
#-*- coding:utf-8 -*-
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class Dlg_Warning():

    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("./UI/guia-1_inter.ui")

        self.warning = builder.get_object("mensaje_advertencia")
        self.warning.set_title("Advertencia: ¿Está seguro?")

        self.warning.show_all()